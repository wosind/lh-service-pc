// var fs = require('fs')
// var copy = function(src, dst) {
//   let paths = fs.readdirSync(src) //同步读取当前目录
//   paths.forEach(function(path) {
//     var _src = src + '/' + path
//     var _dst = dst + '/' + path
//     fs.stat(_src, function(err, stats) {
//       //stats  该对象 包含文件属性
//       if (err) throw err
//       if (stats.isFile()) {
//         //如果是个文件则拷贝
//         let readable = fs.createReadStream(_src) //创建读取流
//         let writable = fs.createWriteStream(_dst) //创建写入流
//         readable.pipe(writable)
//       } else if (stats.isDirectory()) {
//         //是目录则 递归
//         checkDirectory(_src, _dst, copy)
//       }
//     })
//   })
// }
// var checkDirectory = function(src, dst, callback) {
//   fs.access(dst, fs.constants.F_OK, err => {
//     if (err) {
//       fs.mkdirSync(dst)
//       callback(src, dst)
//     } else {
//       callback(src, dst)
//     }
//   })
// }

// const SOURCES_DIRECTORY = 'd:\\a' //源目录
// checkDirectory(SOURCES_DIRECTORY, 'd:\\b', copy)

// const t = require('./.electron-vue/genver')
// console.log(t('abc'))

/************************************************************************
  rule    分段规则
  split   分段间隔字符
  isUper  输出是否转大写
*************************************************************************/
function Guid(_opt) {
  if (_opt == undefined) _opt = {}
  const opt = {
    rule: _opt.rule || [5, 5, 5, 5, 5],
    split: _opt.split || '-',
    isUper: _opt.isUper || 0
  }

  /*
    该方法生成7位以上随机数异常 
    这里拆成每次生成6位及以下的随机数
  */
  const R = bit => {
    const r = bit => {
      return (((Math.random() * (0.9 - 0.1) + 0.1) * 16 ** bit) | 0).toString(16)
    }
    let res = ''
    while (bit > 0) {
      if (bit <= 6) return (res += r(bit))
      bit -= 6
      res += r(6)
    }
  }

  let res = ''
  for (let b of opt.rule) res += res != '' ? opt.split + R(b) : R(b)
  return opt.isUper == 1 ? res.toUpperCase() : res
}

for (let i = 0; i < 9999; i++) {
  console.log(Guid({ rule: [8, 4, 4, 4, 12] }))
}
