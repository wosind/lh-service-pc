// prettierrc.jsmodule.exports = {
//   // 最大长度80个字符
//   printWidth: 200,
//   // 行末分号
//   semi: false,
//   // 单引号
//   singleQuote: true,
//   // JSX双引号
//   jsxSingleQuote: false,
//   // 尽可能使用尾随逗号(包括函数参数)
//   trailingComma: none,  // 在对象文字中打印括号之间的空格。
//   bracketSpacing: true,  // > 标签放在最后一行的末尾，而不是单独放在下一行
//   jsxBracketSameLine: false,  // 箭头圆括号
//   arrowParens: avoid,  // 在文件顶部插入一个特殊的 @format 标记，指定文件格式需要被格式化。
//   insertPragma: false,  // 缩进
//   tabWidth: 2,  // 使用tab还是空格
//   useTabs: false,  // 行尾换行格式
//   endOfLine: auto,
//   HTMLWhitespaceSensitivity: ignore,
// };

// .prettierrc 文件
// 这里修改的都是与默认值不同的，没有修改到的就是启用默认值
// .prettierrc 文件是使用 json 格式，如果报错了，该配置文件在编辑器里面是不会生效的
// {
//   bracketSpacing: true,
//   printWidth: 160,
//   semi: false,
//   singleQuote: true
// }
// 其他还可以选择
// prettier.config.js
module.exports = {
  bracketSpacing: true, // 是否在对象属性添加空格，这里选择是 { foo: bar }
  printWidth: 120, // 指定代码换行的行长度。单行代码宽度超过指定的最大宽度，将会换行，如果都不想换，可以添加 proseWrap: never
  semi: false, // 是否在语句末尾打印分号，这里选择不加
  singleQuote: true // 是否使用单引号，这里选择使用
}
// package.json 中的 prettier 属性
// {
//   prettier: {
//     bracketSpacing: true,   // 是否在对象属性添加空格，这里选择是 { foo: bar }
//     printWidth: 160,        // 指定代码换行的行长度。单行代码宽度超过指定的最大宽度，将会换行，如果都不想换，可以添加 proseWrap: never
//     semi: false,            // 是否在语句末尾打印分号，这里选择不加
//     singleQuote: true      // 是否使用单引号，这里选择使用
//   }
// }
