const path = require('path')
const fs = require('fs')

module.exports = function getCommitDate(env) {
  let now = new Date()
  const pre = function(d) {
    return d < 10 ? `0${d}` : d
  }
  const y = `${now.getFullYear()}`.substring(2, 4)
  const m = pre(now.getMonth() + 1)
  const d = pre(now.getDate())
  const H = pre(now.getHours())
  const M = pre(now.getMinutes())
  const v = `${y}.${m}${d}.${H}${M}`

  if (env == 'production') {
    const packageFile = path.join(path.resolve(__dirname, '..'), 'package.json')
    let content = fs.readFileSync(packageFile, 'utf-8')
    content = content.replace(/"version":[ ]?"([^"]*)"/, `"version": "${v}"`)
    fs.writeFileSync(packageFile, content)
  }
  return v
}
