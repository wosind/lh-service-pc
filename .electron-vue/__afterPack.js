const fs = require('fs')

const copy = function(src, dst) {
  let paths = fs.readdirSync(src) //同步读取当前目录
  paths.forEach(function(path) {
    var _src = src + '/' + path
    var _dst = dst + '/' + path
    fs.stat(_src, function(err, stats) {
      //stats  该对象 包含文件属性
      if (err) throw err
      if (stats.isFile()) {
        //如果是个文件则拷贝
        let readable = fs.createReadStream(_src) //创建读取流
        let writable = fs.createWriteStream(_dst) //创建写入流
        readable.pipe(writable)
      } else if (stats.isDirectory()) {
        //是目录则 递归
        checkDirectory(_src, _dst, copy)
      }
    })
  })
}

const checkDirectory = function(src, dst, callback) {
  fs.access(dst, fs.constants.F_OK, err => {
    if (err) {
      fs.mkdirSync(dst)
      callback(src, dst)
    } else {
      callback(src, dst)
    }
  })
}

exports.default = async function(context) {
  console.log('\n    **copy ghost script and cstamp to package**')

  const dirLis = ['gs', 'clearstamp', 'clodop']
  const projectDir = context.packager.info.projectDir
  const outputDir = context.appOutDir

  dirLis.forEach(dir => {
    let souDir = projectDir + '\\' + dir
    let disDir = outputDir + '\\' + dir
    console.log('    make dir ' + disDir)
    fs.mkdirSync(disDir)
    console.log('    copying ' + souDir + ' to ' + disDir + '\n')
    checkDirectory(souDir, disDir, copy)
  })
}
