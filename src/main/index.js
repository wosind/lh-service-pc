'use strict'

require('console')
console.log('enter index.js')
import { app, BrowserWindow, globalShortcut } from 'electron'
let mainWindow = null
const gotTheLock = app.requestSingleInstanceLock()
if (!gotTheLock) {
  app.quit()
} else {
  if (process.env.NODE_ENV !== 'development') {
    global.__static = require('path')
      .join(__dirname, '/static')
      .replace(/\\/g, '\\\\')
  }
  const winURL = process.env.NODE_ENV === 'development' ? `http://localhost:9080` : `file://${__dirname}/index.html`
  function createWindow() {
    /**
     * Initial window options
     */
    mainWindow = new BrowserWindow({
      useContentSize: true, //窗口width,height 排除title和menu区域的高度、宽度
      frame: false, //去窗口边框
      height: 900,
      width: 1440,
      minHeight: 760,
      minWidth: 1080,
      webPreferences: {
        nodeIntegration: true, //在渲染进程中使用nodejs模块
        webSecurity: false,
        webviewTag: true
      }
    })
    // mainWindow.maximize()
    mainWindow.loadURL(winURL)
    mainWindow.webContents.closeDevTools()

    // Open dev tools initially when in development mode
    if (process.env.NODE_ENV === 'development') {
      mainWindow.closeDevTools()
      // mainWindow.webContents.on('did-frame-finish-load', () => {
      //   mainWindow.webContents.once('devtools-opened', () => {
      //     mainWindow.focus()
      //   })
      //   mainWindow.webContents.openDevTools()
      // })
    }

    mainWindow.on('closed', () => {
      mainWindow = null
    })
  }

  app
    .whenReady()
    .then(() => {
      globalShortcut.register('Alt+W', () => {
        mainWindow.openDevTools()
        console.log('Electron loves global shortcuts!')
        mainWindow.webContents.closeDevTools()
      })
      // globalShortcut.register("Alt+Q", () => {
      //   mainWindow.closeDevTools();
      //   console.log("Electron loves global shortcuts!");
      // });
    })
    .then(createWindow)

  // app.on("ready", createWindow);

  app.on('second-instance', (event, commandLine, workingDirectory) => {
    mainWindow.webContents.closeDevTools()
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', () => {
    mainWindow.webContents.closeDevTools()
    if (mainWindow === null) {
      createWindow()
    }
  })

  app.allowRendererProcessReuse = true
  const { dialog } = require('electron')
  const { ipcMain } = require('electron')
  ipcMain.on('get-file-list', (event, parm) => {
    dialog
      .showOpenDialog(mainWindow, {
        defaultPath: parm || '',
        filters: [{ name: 'Pdfs', extensions: ['pdf'] }],
        properties: ['openFile', 'multiSelections']
      })
      .then(result => {
        event.returnValue = result
      })
      .catch(err => {
        event.returnValue = err
      })
  })

  ipcMain.on('get-dir', (event, res) => {
    dialog
      .showOpenDialog(mainWindow, {
        properties: ['openDirectory']
      })
      .then(result => {
        event.returnValue = result
      })
      .catch(err => {
        event.returnValue = err
      })
  })

  ipcMain.on('reload', (event, res) => {
    mainWindow.webContents.reload()
  })

  ipcMain.on('open-dev-tools', (event, res) => {
    mainWindow.openDevTools()
  })

  ipcMain.on('minimize', (event, res) => {
    mainWindow.minimize()
  })

  ipcMain.on('close-win', (event, res) => {
    console.log('close-win')
    mainWindow.close()
  })
}
