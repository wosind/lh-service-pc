/*
  response 携带数据格式

  {
    method: "POST",
    body: { action: "archive", reportNum: "市-2020-045-201128-003" },
    query: {},
  };

*/

export default [
  // 报告上传
  {
    url: "/Mock/Standard/API/ReportArchive",
    type: "post",
    response: (data) => {
      /*
        归档
        action = "archive"
        reportNum = "编号"

        上传照片
        action = "saveReportImage"
        reportNum = "编号"
        reportImage = “base64编码”
        page = “报告页码”    
      */

      let action = data.body.action;

      console.log("action", action);
      if (action == "archive") {
        return {
          code: 0,
          msg: "成功",
        };
      } else if (action == "saveReportImage") {
        for (var t = Date.now(); Date.now() - t <= 1500; );
        return {
          code: 0,
          msg: "成功",
        };
      }
    },
  },
];
