
const tokens = {
  admin: {
    token: 'admin-token'
  },
  editor: {
    token: 'editor-token'
  }
}

const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin',
    menus: [
      {
        "activeMenu": "",
        "alwaysShow": "True",
        "children": [
          {
            "activeMenu": "",
            "alwaysShow": "",
            "component": "permission/page",
            "hidden": "",
            "icon": "",
            "menuId": "7952c5ec253ddabedcf4b3cdab06517a",
            "name": "PagePermission",
            "noCache": "",
            "path": "page",
            "pid": "48b8d4c350cc6cea01ad923e63e45eae",
            "redirect": "",
            "sort": null,
            "title": "页面授权",
            "type": null
          },
          {
            "activeMenu": "",
            "alwaysShow": "",
            "component": "permission/directive",
            "hidden": "",
            "icon": "",
            "menuId": "800bd93a831df002dcd1830bf6874064",
            "name": "DirectivePermission",
            "noCache": "",
            "path": "directive",
            "pid": "48b8d4c350cc6cea01ad923e63e45eae",
            "redirect": "",
            "sort": null,
            "title": "指令授权",
            "type": null
          },
          {
            "activeMenu": "",
            "alwaysShow": "",
            "component": "permission/role",
            "hidden": "",
            "icon": "",
            "menuId": "0220c646b71a5f0ca7fe050b8e272ec9",
            "name": "RolePermission",
            "noCache": "",
            "path": "role",
            "pid": "48b8d4c350cc6cea01ad923e63e45eae",
            "redirect": "",
            "sort": null,
            "title": "角色授权",
            "type": null
          }
        ],
        "component": "Layout",
        "hidden": "",
        "icon": "lock",
        "menuId": "48b8d4c350cc6cea01ad923e63e45eae",
        "name": "Permission",
        "noCache": "",
        "path": "/permission",
        "pid": "",
        "redirect": "/permission/page",
        "sort": null,
        "title": "授权",
        "type": null
      },
      {
        "activeMenu": null,
        "alwaysShow": "",
        "children": [
          {
            "activeMenu": "",
            "alwaysShow": "",
            "component": "service/msgcenter/index",
            "hidden": "",
            "icon": "wechat",
            "menuId": "5978b2f09fc3b8da958232fab20eda3e1",
            "name": "msgcenter",
            "noCache": "True",
            "path": "service/msgcenter",
            "pid": "0113e0a9ecf68361e1fee3fa237a2c981",
            "redirect": "",
            "sort": null,
            "title": "客服",
            "type": null
          }
        ],
        "component": "Layout",
        "hidden": "",
        "icon": null,
        "menuId": "0113e0a9ecf68361e1fee3fa237a2c981",
        "name": "",
        "noCache": null,
        "path": "/service",
        "pid": "",
        "redirect": "",
        "sort": null,
        "title": null,
        "type": null
      },
      {
        "activeMenu": null,
        "alwaysShow": "",
        "children": [
          {
            "activeMenu": "",
            "alwaysShow": "",
            "component": "icons/index",
            "hidden": "",
            "icon": "icon",
            "menuId": "5978b2f09fc3b8da958232fab20eda3e",
            "name": "Icons",
            "noCache": "True",
            "path": "index",
            "pid": "0113e0a9ecf68361e1fee3fa237a2c98",
            "redirect": "",
            "sort": null,
            "title": "图标",
            "type": null
          }
        ],
        "component": "Layout",
        "hidden": "",
        "icon": null,
        "menuId": "0113e0a9ecf68361e1fee3fa237a2c98",
        "name": "",
        "noCache": null,
        "path": "/icon",
        "pid": "",
        "redirect": "",
        "sort": null,
        "title": null,
        "type": null
      }
    ]
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  // user login
  {
    url: '/vue-element-admin/user/login',
    type: 'post',
    response: config => {
      const { username } = config.body
      const token = tokens[username]

      // mock error
      if (!token) {
        return {
          code: 60204,
          message: 'Account and password are incorrect.'
        }
      }

      return {
        code: 20000,
        data: token
      }
    }
  },

  // get user info
  {
    url: '/vue-element-admin/user/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  // user logout
  {
    url: '/vue-element-admin/user/logout',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]
