let xxplugins = {}
import _public from '../public'
import cfg from '@/component/config.vue'
import md5 from '@/utils/md5'
xxplugins.install = function(Vue, vm) {
  const config = function() {
    if (window.CFG != undefined) {
      // 防止界面重复创建
      window.CFG.open()
      return
    }
    const _create = Vue.extend(cfg)
    const Dom = new _create({
      el: document.createElement('div'),
      parent: vm
    })
    document.body.appendChild(Dom.$el)
  }

  const isEmpty = function(obj) {
    return Object.keys(obj).length > 0 ? false : true
  }

  /**
   * @description 格式化时间
   * @param {String|Number} dateTime 需要格式化的时间戳
   * @param {String} fmt 格式化规则 yyyy:mm:dd|yyyy:mm|yyyy年mm月dd日|yyyy年mm月dd日 hh时MM分等,可自定义组合 默认yyyy-mm-dd
   * @returns {string} 返回格式化后的字符串
   */
  function timeFormat(dateTime = null, fmt = 'yyyy-mm-dd') {
    // 如果为null,则格式化当前时间
    if (!dateTime) dateTime = Number(new Date())
    // 如果dateTime长度为10或者13，则为秒和毫秒的时间戳，如果超过13位，则为其他的时间格式
    if (dateTime.toString().length == 10) dateTime *= 1000
    const date = new Date(dateTime)
    let ret
    const opt = {
      'y+': date.getFullYear().toString(), // 年
      'm+': (date.getMonth() + 1).toString(), // 月
      'd+': date.getDate().toString(), // 日
      'h+': date.getHours().toString(), // 时
      'M+': date.getMinutes().toString(), // 分
      's+': date.getSeconds().toString() // 秒
      // 有其他格式化字符需求可以继续添加，必须转化成字符串
    }
    for (const k in opt) {
      ret = new RegExp(`(${k})`).exec(fmt)
      if (ret) {
        fmt = fmt.replace(ret[1], ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, '0'))
      }
    }
    return fmt
  }

  /**
   * @description 时间戳转为多久之前
   * @param {String|Number} timestamp 时间戳
   * @param {String|Boolean} format
   * 格式化规则如果为时间格式字符串，超出一定时间范围，返回固定的时间格式；
   * 如果为布尔值false，无论什么时间，都返回多久以前的格式
   * @returns {string} 转化后的内容
   */
  function zhTime(timestamp = null, format = 'mm-dd') {
    if (timestamp == null) timestamp = Number(new Date())
    timestamp = parseInt(timestamp)
    // 判断用户输入的时间戳是秒还是毫秒,一般前端js获取的时间戳是毫秒(13位),后端传过来的为秒(10位)
    if (timestamp.toString().length == 10) timestamp *= 1000
    let timer = new Date().getTime() - timestamp
    timer = parseInt(timer / 1000)
    // 如果小于5分钟,则返回"刚刚",其他以此类推
    let tips = ''
    switch (true) {
      case timer < 300:
        tips = '刚刚'
        break
      case timer >= 300 && timer < 3600:
        tips = `${parseInt(timer / 60)}分钟前`
        break
      case timer >= 3600 && timer < 86400:
        tips = `${parseInt(timer / 3600)}小时前`
        break
      // case timer >= 86400 && timer < 2592000:
      //   tips = `${parseInt(timer / 86400)}天前`
      //   break
      default:
        // 如果format为false，则无论什么时间戳，都显示xx之前
        if (format === false) {
          if (timer >= 2592000 && timer < 365 * 86400) {
            tips = `${parseInt(timer / (86400 * 30))}个月前`
          } else {
            tips = `${parseInt(timer / (86400 * 365))}年前`
          }
        } else {
          tips = timeFormat(timestamp, format)
        }
    }
    return tips
  }

  Vue.prototype.$Ver = process.env.VER
  Vue.prototype.$Voice = new Audio(`${__static}/02.mp3`)
  Vue.prototype.$now = () => timeFormat(new Date(), 'yyyy-mm-dd hh:MM:ss')
  Vue.prototype.$ebus = new Vue()
  Vue.prototype.$trim = _str => _str.replace(/^\s*|\s*$/g, '')
  Vue.prototype.$Guid = _public.Guid
  Vue.prototype.$Guid20 = _public.Guid20
  Vue.prototype.$md5 = md5
  Vue.prototype.$isEmpty = isEmpty
  Vue.prototype.$zhTime = zhTime
  Vue.prototype.$stampAtNow = function() {
    return `${Math.round(new Date() / 1000)}`
  }
  Vue.prototype.$config = config
}
export default xxplugins
