// // 获取当前的时间戳
// export function NowStamp() {
//   return `${Math.round(new Date() / 1000)}`;
// }

// // 读取 localStorage 并转对象类型
// export function GetItemToObj(key) {
//   let item = localStorage.getItem(key);
//   return item ? JSON.parse(item) : {};
// }

// // 保存信息到localStorage
// export function SetItem(key, obj) {
//   localStorage.setItem(key, JSON.stringify(obj));
// }

export default {
  Guid20: () => {
    let R = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
    }
    return `${R()}${R()}${R()}${R()}`
  },
  //  4位随机字符串
  Guid: () => {
    let R = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
    }
    return `${R()}-${R()}-${R()}-${R()}-${R()}`
  },
  // 保存信息到localStorage
  LS: {
    SetItem: (key, obj) => {
      localStorage.setItem(key, JSON.stringify(obj))
    },
    GetItem: key => {
      return localStorage.getItem(key)
    },
    GetItemToObj: key => {
      let item = localStorage.getItem(key)
      return item ? JSON.parse(item) : {}
    }
  }
}
