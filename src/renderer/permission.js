import router from './router'
import store from './store'
import { Message } from 'element-ui'

const whiteList = ['/login'] // 重定向白名单
router.beforeEach(async (to, from, next) => {
  // console.log(`【${from.path}】跳转到【${to.path}】`)

  if (store.getters['isLogin']) {
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      // console.log('router.beforeEach hasStation')
      let hasRoles = true
      if (hasRoles) {
        // console.log('hasRoles to next')
        next()
      } else {
      }
    }
  } else {
    // 缺少站点信息或用户信息
    if (whiteList.indexOf(to.path) !== -1) {
      //  重定向白名单，不登录 直接跳转
      console.log(`白名单直达`)
      next()
    } else {
      // console.log(`重定向跳往登录页面`)
      next(`/login?redirect=${to.path}`)
    }
  }
})

router.afterEach((to, from) => {
  store.commit('router/update', to)
  // console.log('router跳转结束')
})
