// import router from '@/router'

const data = localStorage.getItem('userinfo')
// {"uid":"8001","uname":"客服01","password":"111","urole":"客服","phone":"138000000001","phoneother":"136"}
let initInfo = {
  uid: '',
  uname: '',
  password: ''
}
if (data) initInfo = JSON.parse(data)
const state = () => ({
  info: initInfo,
  isLogin: false
})

const mutations = {
  loginIn(state, data) {
    state.info.uid = data.uid
    state.info.uname = data.uname
    state.info.pwd = data.pwd
    state.isLogin = true
    localStorage.setItem('userinfo', JSON.stringify(state.info))
  },

  updateInfo(state, data) {
    state.info = data
    localStorage.setItem('userinfo', JSON.stringify(state.info))
  },

  loginOut(state) {
    state.isLogin = false
  }
}

const actions = {
  loginIn({ commit }, data) {
    commit('loginIn', data)
  },
  loginOut({ commit }) {
    commit('loginOut')
  },
  updateInfo({ commit }, data) {
    commit('updateInfo', data)
  }
}
export default { state, mutations, actions }
