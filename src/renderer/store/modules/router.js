const state = () => ({
  path:"",name:"TEST"
})

const getters = {
  path: (state) => state.path,
  name:(state) => state.name
}

const mutations = {
  update(state, router) {
    state.path = router.path,
    state.name = router.name
  }
}

const actions = {
  update({ commit }) {
    commit('update')
  },
}
export default { state, getters, mutations, actions }