const state = {
  status: 'lineOut', //connecting  connected  lineOut waitingConnect
  tips: ''
}

const getters = {
  connectStatus: state => state.status,
  connectTips: state => state.tips
}

const mutations = {
  update(state, data) {
    console.log('updateStatus:', data)
    const { status, tips } = data
    state.status = status
    state.tips = tips
  }
}

const actions = {
  update({ commit }, data) {
    commit('update', data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
