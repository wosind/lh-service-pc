let data = localStorage.getItem('sqls')
const _default = [
  'select * from z_session ',
  'select * from z_service_msg ',
  `select 
	z_session.customer_id  as '客户ID', 
	z_session.customer  as '客户', 
	z_session.openid  as '客户微信ID', 
	z_session.create_dt  as '创建日期', 
	z_session.connect_dt  as '链接日期', 
	z_session.close_dt  as '关闭时间', 
	z_session.session_state  as '会话状态', 
	z_session.servicer_id  as '客服ID', 
	z_session.ischanged  as '预留字段1',
	z_ask.aid  as '问题编号', 
	z_ask.IsBind  as '问题处理状态', 
	CASE WHEN z_ask.IsBind  ='Y' THEN '正在处理' WHEN z_ask.IsBind  ='N' THEN '未接待' ELSE '已经关闭' END  AS '状态',
	z_ask.Ask  as '客户问题', 
	z_ask.Sender  as '账套名', 
	z_ask.Phone  as '客户联系电话', 
	z_ask.IsDependRemote  as '预留字段2', 
	z_ask.RemoteType  as '预留字段:远程方式', 
	z_ask.RemoteAccount  as '向日葵账号', 
	z_ask.RemoteAuth  as '向日葵密码', 
	z_ask.IsWinAuth  as '是否WIN', 
	z_ask.WinUser  as 'WIN用户', 
	z_ask.WinPwd  as 'WIN密码', 
	z_ask.rate  as '评分'
FROM z_ask,z_session
WHERE 	z_session.servicer_id='1001'  AND
		z_ask.session_id=z_session.session_id`
]
const initData = data ? JSON.parse(data) : _default

const state = {
  sqlList: initData
}

const getters = {
  lis: state => state.sqlList
}

const mutations = {
  addItem(state, data) {
    if (state.sqlList.indexOf(data) == -1) state.sqlList.unshift(data)
    localStorage.setItem('sqls', JSON.stringify(state.sqlList))
  },
  removeItem(state, data) {
    state.sqlList.splice(data, 1)
    localStorage.setItem('sqls', JSON.stringify(state.sqlList))
  },
  clear(state, data) {
    state.sqlList = _default
    localStorage.setItem('sqls', JSON.stringify(state.sqlList))
  }
}

const actions = {
  addItem({ commit }, data) {
    commit('addItem', data)
  },
  clear({ commit }, data) {
    commit('clear')
  },
  removeItem({ commit }, data) {
    commit('removeItem', data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
