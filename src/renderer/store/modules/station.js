import _public from '@/utils/public'
let data = localStorage.getItem('station')

let _info = {}
if (data) {
  _info = JSON.parse(data)
} else {
  const _id = _public.Guid()
  _info = { id: _id, name: _id }
  localStorage.setItem('station', JSON.stringify(_info))
}

const state = {
  info: _info,
  wsid: ''
}

const getters = {
  stationName: state => state.info.name
}

const mutations = {
  updateName(state, name) {
    state.info.name = name
    localStorage.setItem('station', JSON.stringify(state.info))
  },
  updateWsid(state, id) {
    state.wsid = id
  }
}

const actions = {
  updateName({ commit }, data) {
    commit('updateName', data)
  },
  updateWsid({ commit }, data) {
    commit('updateWsid', data)
  }
}

export default { state, getters, mutations, actions }
