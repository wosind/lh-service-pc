const _host = localStorage.getItem('host')

const state = {
  host: _host || 'https://ser.lanhuasoft.com/'
}

const mutations = {
  set(state, data) {
    state.host = data
    localStorage.setItem('host', data)
  }
}

const actions = {
  set({ commit }, data) {
    commit('set', data)
  }
}

export default {
  state: state,
  mutations: mutations,
  actions: actions
}
