// {
//   ErrCode: '0',
//   Err: '正常接入',
//   session_id: '5407e4bf-5c7b-4ad6-8f67-f078cb8e8ff1',
//   Detail: {
//        customer: '占陇奥升服饰',
//        customer_id: '202002241705001',
//        createTime: '1646787767',
//        content: 'q' }
// }

// let s = {
//   sessionId: 'db816724-7527-4ded-96de-aa5c45e6e3c1',
//   orderNum: 1,
//   customerId: '202002241705001',
//   dir: 'c2s',
//   addTime: 0,
//   state: 0,
//   toUserName: 'gh_d3796fc58380',
//   fromUserName: 'oxfnA4rmvKzIUz1jlWFIeYbMssUE',
//   createTime: 1646805117,
//   event: '',
//   sessionFrom: '',
//   msgType: 'text',
//   msgId: 23576581849922730,
//   content: '1',
//   picUrl: '',
//   mediaId: '',
//   title: '',
//   appId: '',
//   pagePath: '',
//   thumbUrl: '',
//   thumbMediaId: ''
// }

const state = () => ({
  noJoinList: {},
  activeList: {},
  closedList: {},
  currentSession: '',
  customerInfo: {},
  msgList: []
})

const mutations = {
  updateActionListCustomer(state, data) {
    const { SessionId, customer } = data
    state.activeList[SessionId]['customer'] = customer
  },

  updateActiveList(state, data) {
    const { SessionId, items } = data
    const { Content, CreateTime, isread } = items
    state.activeList[SessionId]['lastMsg'] = Content
    state.activeList[SessionId]['lastMsgSendTime'] = CreateTime
    state.activeList[SessionId]['isread'] = isread
  },

  markReadStaus(state, SessionId) {
    state.activeList[SessionId]['isread'] = '1'
  },

  // 以 noJoinList为例
  // noJoinList{
  //   "sessionId01":{item}
  // }
  //  listType = "noJoinList为例"
  //  sessionId = "sessionId01"
  //  item = item obj
  addItem(state, data) {
    let { listType, sessionId, item } = data
    state[listType][sessionId] = item
  },

  resetList(state, data) {
    let { listType, item } = data
    state[listType] = item
  },

  removeItem(state, data) {
    let { listType, sessionId } = data
    delete state[listType][sessionId]
  },
  setCurrentSession(state, data) {
    state.currentSession = data
  },
  setCustomerInfo(state, data) {
    state.customerInfo = data
  },
  updateCustomerSubInfo(state, data) {
    state.customerInfo['subinfo'] = data
  },
  initMsgList(state, data) {
    state.msgList = data
  },
  addMsg(state, data) {
    state.msgList.push(data)
  },
  receivedMsg(state, data) {
    state.msgList.push(data)
  },

  clearMsgList(state) {
    state.msgList = []
  },
  updateSendState(state, data) {
    const sid = data.session_id
    const cid = data.callBackId
    const _state = data.state
    if (sid == state.currentSession) {
      for (let inx = 0; inx < state.msgList.length; inx++) {
        if (cid == state.msgList[inx].callBackId) {
          if (_state == 'sucess') {
            state.msgList[inx].State = 1
          } else {
            state.msgList[inx].State = 100
            state.msgList[inx].err = data.statedetail
          }
          break
        }
      }
    }
  }
}

const actions = {
  updateCustomerSubInfo({ commit }, data) {
    commit('updateCustomerSubInfo', data)
  },
  markReadStaus({ commit }, sid) {
    commit('markReadStaus', sid)
  },

  updateActionListCustomer({ commit }, data) {
    commit('updateActionListCustomer', data)
  },

  clearMsgList({ commit }, data) {
    commit('clearMsgList')
  },
  updateSendState({ commit }, data) {
    commit('updateSendState', data)
  },
  addMsg({ commit }, data) {
    commit('addMsg', data)

    let { SessionId, Content, MsgType, CreateTime } = data
    if (MsgType == 'image') {
      Content = '收到图片'
    }
    commit('updateActiveList', { SessionId, items: { Content, CreateTime, isread: '1' } })

    return data.callBackId
  },

  updateActiveList({ commit }, data) {
    commit('updateActiveList', data)
  },
  receivedMsg({ commit }, data) {
    commit('receivedMsg', data)
  },
  initMsgList({ commit }, data) {
    commit('initMsgList', data)
    return true
  },

  setCurrentSession({ commit }, data) {
    commit('setCurrentSession', data)
  },
  setCustomerInfo({ commit }, data) {
    commit('setCustomerInfo', data)
    return
  },

  addItem({ commit }, data) {
    commit('addItem', data)
  },
  resetList({ commit }, data) {
    commit('resetList', data)
  },
  removeItem({ commit }, data) {
    commit('removeItem', data)
  }
}

const getters = {
  closedList: state => state.closedList,
  noJoinList: state => state.noJoinList,
  activeList: state => state.activeList,
  currentSession: state => state.currentSession,
  customerInfo: state => state.customerInfo,
  msgList: state => state.msgList,
  imgPreviewList: state => {
    let lis = []
    state.msgList.forEach(e => {
      if (['link', 'image'].indexOf(e['MsgType']) > -1) {
        lis.push(e['PicUrl'] || e['ThumbUrl'] || e['ImgBase64'])
      }
    })
    return lis
  }
}
export default {
  state,
  getters,
  mutations,
  actions
}
