const getters = {
  sid: state => state.station.info.id,
  sname: state => state.station.info.name,
  uid: state => state.user.info.uid,
  uname: state => state.user.info.uname,
  pwd: state => state.user.info.pwd,
  isLogin: state => state.user.isLogin,
  connectStaus: state => state.connection.status,
  currentPath: state => state.router.path,
  host: state => state.config.host,
  wsHost: state => `${state.config.host.replace(/https*(?=:)/g, 'wss')}msgHub`,
  wsid: state => state.station.wsid
}

export default getters
