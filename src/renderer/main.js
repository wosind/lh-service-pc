// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'
import router from './router'

import store from './store'

import './permission'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI, { size: 'mini' })

import { DocNote, DocNoteConfig } from 'doc-note'
DocNoteConfig({
  languages: ['SQL'],
  theme: 'default' // ['default', 'coy', 'dark', 'funky', 'okaidia', 'solarizedlight', 'tomorrow', 'twilight']
})
console.log('render index.js')
console.log(process.env)
Vue.use(DocNote)

import _public from './utils/public'
Vue.prototype.PUBLIC = _public
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.prototype.$EBS = new Vue()
const app = new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})

import _plugin from './utils/plugins'
Vue.use(_plugin, app)
