import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/views/layout'

Vue.use(Router)

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index')
  },
  {
    path: '/',
    redirect: '/home',
    component: Layout,
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: '客服会话'
      },
      {
        path: 'query',
        component: () => import('@/views/query/index'),
        name: '统计汇总'
      },
      {
        path: 'devhelper',
        component: () => import('@/views/devhelper/index'),
        name: '开发助手'
      }
    ]
  },
  {
    path: '*',
    redirect: '/login',
    hidden: true
  }
]

const router = createRouter()
function createRouter(routes = constantRoutes) {
  return new Router({
    routes: routes
  })
}

// const originalPush = Router.prototype.push;
// Router.prototype.push = function push(location, onResolve, onReject) {
//     if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
//     return originalPush.call(this, location).catch(err => err);
// };
export default router
