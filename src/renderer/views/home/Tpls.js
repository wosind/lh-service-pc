export const msgTpl = {
  SessionId: null,
  OrderNum: null,
  Dir: 's2c',
  CustomerId: null,
  AddTime: null,
  State: -1,
  ToUserName: null,
  FromUserName: null,
  CreateTime: null,
  Event: null,
  SessionFrom: null,
  MsgType: null,
  MsgId: null,
  Content: null,
  PicUrl: null,
  MediaId: null,
  Title: null,
  AppId: null,
  PagePath: null,
  ThumbUrl: null,
  ThumbMediaId: null,
  callBackId: null,
  ImgBase64: null
}

export const actionTpl = {
  action: '',
  session_id: '',
  userid: '',
  password: '',
  StationId: '',
  Station: '',
  callBackId: '',
  msgtext: '',
  msg: ''
}
